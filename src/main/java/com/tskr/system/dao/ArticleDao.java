/**  
 * Project Name:36kr  
 * File Name:ArticleDao.java  
 * Package Name:com.tskr.system.dao  
 * Date:2017年11月19日下午1:56:06  
 * Copyright (c) 2017, lys@fenla.net All Rights Reserved.  
 *  
*/  
  
package com.tskr.system.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;

import com.tskr.common.bean.Article;

/**  
 * ClassName:ArticleDao <br/>  
 * Function: TODO <br/>  
 * Date:     2017年11月19日 下午1:56:06 <br/>  
 * @author   lys
 * @version    
 * @since    JDK 1.8 
 * @see        
 */
public interface ArticleDao {
	/**
	 * queryList:获取数据库中全部数据
	 * @author lys
	 * @return
	 */
	List<Article> queryList(@Param("start") int start, @Param("count") int count);
	
	/**
	 * queryListByClassOrLabel:条件查询
	 * @author lys
	 * @param classname
	 * @param labelname
	 * @return
	 */
	List<Article> queryListByClassOrLabel(@Param("className") String classname,@Param("labelName")String labelname,@Param("start") int start, @Param("count") int count);

}
  
