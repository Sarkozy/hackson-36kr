/**  
 * Project Name:36kr  
 * File Name:ArticleServiceImpl.java  
 * Package Name:com.tskr.system.service  
 * Date:2017年11月19日下午2:17:53  
 * Copyright (c) 2017, lys@fenla.net All Rights Reserved.  
 *  
*/  
  
package com.tskr.system.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tskr.common.bean.Article;
import com.tskr.system.dao.ArticleDao;

/**  
 * ClassName:ArticleServiceImpl <br/>  
 * Function: TODO <br/>  
 * Date:     2017年11月19日 下午2:17:53 <br/>  
 * @author   lys
 * @version    
 * @since    JDK 1.8 
 * @see        
 */
@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
    private ArticleDao articleDao;

	public List<Article> getAll(int page) {
		return articleDao.queryList((page-1) * 10, 10);
	}

	public List<Article> getByCondiction(String classname, String labelname, int start, int count) {
		return articleDao.queryListByClassOrLabel(classname, labelname, start, count);
	}
	
	

}
  
