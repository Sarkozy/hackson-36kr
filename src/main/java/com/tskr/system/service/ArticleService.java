/**  
 * Project Name:36kr  
 * File Name:ArticleService.java  
 * Package Name:com.tskr.system.service  
 * Date:2017年11月19日下午2:17:11  
 * Copyright (c) 2017, lys@fenla.net All Rights Reserved.  
 *  
*/  
  
package com.tskr.system.service;  
/**  
 * ClassName:ArticleService <br/>  
 * Function: TODO <br/>  
 * Date:     2017年11月19日 下午2:17:11 <br/>  
 * @author   lys
 * @version    
 * @since    JDK 1.8 
 * @see        
 */

import java.util.List;


import com.tskr.common.bean.Article;

 
public interface ArticleService {
	
	List<Article> getAll(int page);
	
	List<Article> getByCondiction(String classname,String labelname,int start,int count);

}
  
