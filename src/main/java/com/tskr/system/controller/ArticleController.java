/**  
 * Project Name:36kr  
 * File Name:ArticleController.java  
 * Package Name:com.tskr.system.controller  
 * Date:2017年11月19日下午2:27:01  
 * Copyright (c) 2017, lys@fenla.net All Rights Reserved.  
 *  
*/

package com.tskr.system.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tskr.common.bean.Article;
import com.tskr.system.service.ArticleService;

/**
 * ClassName:ArticleController <br/>
 * Function: TODO <br/>
 * Date: 2017年11月19日 下午2:27:01 <br/>
 * 
 * @author lys
 * @version
 * @since JDK 1.8
 * @see
 */
@Controller
@RequestMapping(value = "article")
public class ArticleController {

	@Autowired
	private ArticleService articleService;

	@RequestMapping(value = "getAll")
	public ModelAndView getAll(@RequestParam(value = "page", defaultValue = "1") int page) {
		List<Article> articleList = new ArrayList<Article>();
		articleList = articleService.getAll(page);
		ModelAndView mv = new ModelAndView();
		mv.addObject("articleList",articleList);
		mv.addObject("page", page);
		mv.setViewName("article");
		return mv;
	}
	
	@RequestMapping(value = "getCondiction")
	public ModelAndView getCondiction(String classname, String labelname, int start, int count) {
		List<Article> articleList = new ArrayList<Article>();
		articleList = articleService.getByCondiction(classname, labelname, start, count);
		ModelAndView mv = new ModelAndView();
		mv.addObject("articleList",articleList);
		mv.setViewName("article");
		return mv;
	}
}
