/**  
 * Project Name:36kr  
 * File Name:Article.java  
 * Package Name:com.tskr.common.bean  
 * Date:2017年11月19日下午1:44:17  
 * Copyright (c) 2017, lys All Rights Reserved.  
 *  
*/  
  
package com.tskr.common.bean;

import org.apache.ibatis.type.Alias;

/**  
 * ClassName:Article <br/>  
 * Function: TODO <br/>  
 * Date:     2017年11月19日 下午1:44:17 <br/>  
 * @author   lys
 * @version    
 * @since    JDK 1.8 
 * @see        
 */
@Alias("Article")
public class Article {
	/**
	 * 文章ID
	 */
	private String id;
	
	/**
	 * 文章标题
	 */
	private String title;
	
	/**
	 * 文章简介
	 */
	private String content;
	
	/**
	 * 文章类型
	 */
	private String classname;
	
	/**
	 * 文章标签
	 */
	private String label;
	
	/**
	 * 发表时间
	 */
	private String time;
	
	/**
	 * 图片地址
	 */
	private String picurl;
	
	/**
	 * 文章地址
	 */
	private String arturl;

	/**
	 * @return 
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param 
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param 
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return 
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param 
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return 
	 */
	public String getClassname() {
		return classname;
	}

	/**
	 * @param 
	 */
	public void setClassname(String classname) {
		this.classname = classname;
	}

	/**
	 * @return 
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param 
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return 
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param 
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return 
	 */
	public String getPicurl() {
		return picurl;
	}

	/**
	 * @param 
	 */
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}

	/**
	 * @return 
	 */
	public String getArturl() {
		return arturl;
	}

	/**
	 * @param 
	 */
	public void setArturl(String arturl) {
		this.arturl = arturl;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", content=" + content + ", classname=" + classname
				+ ", label=" + label + ", time=" + time + ", picurl=" + picurl + ", arturl=" + arturl + "]";
	}

	/**
	 * TODO(这里用一句话描述这个类的作用)
	 */
	public Article() {
		super();
	}

	/**
	 * TODO(这里用一句话描述这个类的作用)
	 * @param id
	 * @param title
	 * @param content
	 * @param classname
	 * @param label
	 * @param time
	 * @param picurl
	 * @param arturl
	 */
	public Article(String id, String title, String content, String classname, String label, String time, String picurl,
			String arturl) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.classname = classname;
		this.label = label;
		this.time = time;
		this.picurl = picurl;
		this.arturl = arturl;
	}
	
	
	

}
  
