<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>36kr创投信息跟踪系统</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
	<script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<style type="text/css">
		* {
			font-family: 微软雅黑;
		}
		
		body {
			background-color: #ecf0f1;
			padding-top: 30px;
		}
	</style>
</head>
<body>
	<!--布局容器-->
	<div class="container">
		<header class="page-header">
		</header>
			
			<!--导航条-->
			<nav class='navbar navbar-inverse navbar-fixed-top'>
				<div class="container">
					<div class="navbar-header">
						<a href="" class='navbar-brand'>
							36kr创投信息跟踪系统
						</a>
						<button class='navbar-toggle collapsed' data-toggle='collapse' data-target='#mynavbar'>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>	
	
					<div id="mynavbar" class='collapse navbar-collapse'>
						<ul class='nav navbar-nav'>
							<li><a href="">首页</a></li>
							<li class='active'><a href="">创投信息</a></li>
						</ul>
						
					</div>
				</div>
			</nav>
			<!--导航条结束-->
		<!--页面主体-->
		<div class="row">
			<!--左侧菜单-->
		<!-- 	<div class="col-md-1">
				<!--左侧菜单卡-->
			<!--	<div class="row">
				<div class="col-md-12">		
					<div class="list-group">
						<a href="#" class="list-group-item active">分类关键词</a>
						<a href="#" class="list-group-item">分类关键词</a>
						<a href="#" class="list-group-item">分类关键词</a>
					</div>				
					</div> 
				</div>-->
				<!--左侧菜单卡结束-->
			</div>
			<!--右侧界面-->
			<div class="col-md-11">
				<!--内容卡片-->
				<div class="row">
					<div class="col-md-12">
											
					<c:forEach items="${requestScope.articleList}" var="article">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="media">
									<div class="media-left" href="#">
										<img class="media-object img-rounded" src="${article.picurl}" alt="...">
									</div>
									<div class="media-body">
										<h3 class="media-heading" ><a href="${article.arturl}" target="_blank"  style="text-decoration:none">${article.title}</a>​</h3>
										<p class="text-muted">${article.content}</p>
										<div>${article.time}</div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
						
						
						<!--分页按钮开始-->
						<ul class='pager'>
							<li>
								<a href="${pageContext.request.contextPath}/article/getAll">首页</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/article/getAll?page=${page+1}">下一页</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/article/getAll?page=${page-1 == 0 ? 1 : page-1}">上一页</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/article/getAll">尾页</a>
							</li>
						</ul>
						<!--分页按钮结束-->
						
						
					</div>
				</div>
				<!--内容卡片结束-->
			</div>
			<!--右侧界面结束-->
		</div>
		<!--页面主体结束-->
		<!--页脚开始-->
		<div class="modal-footer">
			<p class="text-center">36kr创投信息跟踪系统</p>
			<p class="text-center">Copyright ©2017 trkr. All rights reserved.</p>
		</div>
		<!--页脚结束-->
	</div>
	<!--布局容器结束-->
</body>
</html>