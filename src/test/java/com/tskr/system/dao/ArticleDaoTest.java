/**  
 * Project Name:36kr  
 * File Name:ArticleDaoTest.java  
 * Package Name:com.tskr.system.dao  
 * Date:2017年11月19日下午3:38:46  
 * Copyright (c) 2017, lys@fenla.net All Rights Reserved.  
 *  
*/  
  
package com.tskr.system.dao;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tskr.system.dao.ArticleDao;


/**  
 * ClassName:ArticleDaoTest <br/>  
 * Function: TODO <br/>  
 * Date:     2017年11月19日 下午3:38:46 <br/>  
 * @author   lys
 * @version    
 * @since    JDK 1.8 
 * @see        
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-dao.xml")
public class ArticleDaoTest {
	
	@Resource
    private ArticleDao articleDao;

	@Test
	public void testQueryList() {
		articleDao.queryList(0, 5);
	}

	@Test
	public void testQueryListByClassOrLabel() {
		fail("Not yet implemented");
	}

}
  
